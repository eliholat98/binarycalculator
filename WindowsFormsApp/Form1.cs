﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Calculator;

namespace WindowsFormsApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            lastFocused = textBox1;
        }

        TextBox lastFocused;

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "/";
        }
        private void button2_Click(object sender, EventArgs e)
        {
            lastFocused.Text += "0";
        }
        private void button3_Click(object sender, EventArgs e)
        {
            lastFocused.Text += "1";
        }
        private void btEql_Click(object sender, EventArgs e)
        {
            string variable1 = textBox1.Text;
            string variable2 = textBox2.Text;
           var calc = new BinaryCalculator();

            if (label1.Text == "/")
            {
                string z = calc.Zeros(variable2);
                if (z == "true")
                {
                    MessageBox.Show(
                   "На ноль делить нельзя",
                   "Сообщение",
                   MessageBoxButtons.OK,
                   MessageBoxIcon.Information);
                }
                else
                {
                    DivResult res = calc.division1(variable1, variable2);
                    label2.Text = res.Result + " остаток от деления " + res.Reminder;
                }
                
            }
            else if (label1.Text == "+")
            {
                string res = calc.Addition(variable1, variable2);
                label2.Text = res;
            }
            else if (label1.Text == "-")
            {
                string res = calc.Subtraction(variable1, variable2);
                label2.Text = res;
            }
            else if (label1.Text == "*")
            {
                string res = calc.Multiplication(variable1, variable2);
                label2.Text = res;
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            label1.Text = "+";
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            lastFocused = textBox1;
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            lastFocused = textBox2;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            label1.Text = "-";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            label1.Text = "*";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            string a = textBox1.Text;
            string b = textBox2.Text;
            var calc = new BinaryCalculator();
            string res = calc.Comparison(a, b);
            if (res == "equally")
            {
                label2.Text = "Число " + a + " = числу " + b;
            }
            else if (res == "more")
            {
                label2.Text = "Число " + a + " > числa " + b;
            }
            else if (res == "less")
            {
                label2.Text = "Число " + a + " < числa " + b;
            }


        }
    }
}

